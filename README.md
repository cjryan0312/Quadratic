# About
This is a simple little program to do the quadratic formula, show how many real answers there are, shows factored and unfactored equation, and also does standard to vertex form. It also has a GUI if you don't want to use the calculator on the command line.
